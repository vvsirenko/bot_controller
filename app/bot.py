from telegram import BotCommand, Update, InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup, \
    KeyboardButton, ReplyKeyboardRemove
from telegram.constants import ParseMode
from telegram.ext import ContextTypes, Application, ApplicationBuilder, CommandHandler, ConversationHandler, \
    MessageHandler, filters, CallbackQueryHandler
from aiohttp.connector import TCPConnector
from aiohttp import ClientSession

from app.logging_settings import logger
from app.templates.messages import riddle_format_template
from app.utils import create_riddle_text_post, split_riddle_answer


class ChatTelegramBot:
    """
    Class Telegram Bot
    """
    # stages
    CALL_CHANNELS = "call_channels"
    CALL_CHOISE_CHANNEL = "call_choise_channel"
    CALL_SAVE_POST = "call_save_post"
    CALL_CREATE_POST = "call_create_post"
    CALL_SEND_POST = "call_send_post"
    CALL_NEW_POST = "call_new_post"
    CALL_CREATE_POST_ = "call_create_post_"

    def __init__(
            self,
            config: dict,
            integration_client = None
    ):
        self.config = config
        self.commands = [
            BotCommand(
                command='start',
                description='Обновить меню'
            ),
            BotCommand(
                command='end',
                description='Закончить'
            )
        ]
        self.integration_client = integration_client
        self.usage = {}
        self.last_message = {}
        self.riddle_channel_id = self.config.get('riddle_chat_id')

    async def start(self, update: Update, _: ContextTypes.DEFAULT_TYPE) -> str:
        whitelist: list = self.config['whitelist']
        if update.effective_user.id not in whitelist:
            logger.info("user %s try to run bot, user_id %s", update.effective_user.name, update.effective_user.id)
            pass

        start_text = f"Выбери действие нажав на кнопку"
        reply_keyboard = [
            [
                KeyboardButton("Создать пост")
            ]
        ]
        keyboard = ReplyKeyboardMarkup(
            keyboard=reply_keyboard,
            resize_keyboard=True
        )
        await update.message.reply_text(start_text, reply_markup=keyboard)
        return self.CALL_CHANNELS

    async def channels(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        start_text = "Выбери тип поста, нажми на кнопку"
        reply_keyboard = [
            [
                KeyboardButton("Загадки")
            ],
            [KeyboardButton("Закончить")]
        ]
        keyboard = ReplyKeyboardMarkup(
            keyboard=reply_keyboard,
            resize_keyboard=True
        )
        await update.message.reply_text(start_text, reply_markup=keyboard)
        return self.CALL_CHOISE_CHANNEL

    async def get_post(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        await update.message.reply_markdown(riddle_format_template)
        return self.CALL_SAVE_POST

    async def save_post(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        try:
            key = "user_post_text"
            value = update.message.text
            riddle, _ = split_riddle_answer(value)
            context.user_data[key] = split_riddle_answer(value)
            send_text = create_riddle_text_post(riddle)

            reply_keyboard = [
                [
                    KeyboardButton("Добавить кнопку")
                ],
                [KeyboardButton("Закончить")]
            ]
            keyboard = ReplyKeyboardMarkup(
                keyboard=reply_keyboard,
                resize_keyboard=True
            )
            await update.message.reply_markdown(send_text, reply_markup=keyboard)
            return self.CALL_CREATE_POST
        except ValueError as ex:
            logger.error("Exception ex: %s", ex)
            reply_keyboard = [
                [
                    KeyboardButton("Попробовать еще раз")
                ],
                [KeyboardButton("Закончить")]
            ]
            keyboard = ReplyKeyboardMarkup(
                keyboard=reply_keyboard,
                resize_keyboard=True
            )
            await update.message.reply_markdown("Неверный формат", reply_markup=keyboard)
            return self.CALL_CREATE_POST_

    async def create_post(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        start_text = "Проверить пост:"
        post_test: list = context.user_data["user_post_text"]
        riddle, answer = post_test
        riddle = create_riddle_text_post(riddle)

        keyboard = [
            [InlineKeyboardButton("Отгадка", callback_data=answer)],
        ]

        reply_markup = InlineKeyboardMarkup(keyboard)

        reply_keyboard = [
            [
                KeyboardButton("Опубликовать пост")
            ],
            [KeyboardButton("Закончить")]
        ]
        reply_keyboard = ReplyKeyboardMarkup(
            keyboard=reply_keyboard,
            resize_keyboard=True
        )
        await update.message.reply_markdown(riddle, reply_markup=reply_markup)
        await update.message.reply_markdown("публикуем?", reply_markup=reply_keyboard)
        return self.CALL_SEND_POST

    async def send_post(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        post_test: list = context.user_data["user_post_text"]
        riddle, answer = post_test
        riddle = create_riddle_text_post(riddle)

        keyboard = [
            [InlineKeyboardButton("Отгадка", callback_data=answer)],
        ]
        reply_markup = InlineKeyboardMarkup(keyboard)

        await context.bot.send_message(
            chat_id=self.riddle_channel_id,
            text=riddle,
            reply_markup=reply_markup,
            parse_mode=ParseMode.MARKDOWN_V2
        )
        reply_keyboard = [
            [
                KeyboardButton("Новый пост")
            ],
            [KeyboardButton("Закончить")]
        ]
        reply_keyboard = ReplyKeyboardMarkup(
            keyboard=reply_keyboard,
            resize_keyboard=True
        )
        await update.message.reply_markdown("Отправлено. Новый пост?", reply_markup=reply_keyboard)
        logger.info("message send to channel_id %s", self.riddle_channel_id)
        return self.CALL_NEW_POST

    async def post_init(self, application: Application) -> None:
        connector: TCPConnector = TCPConnector(limit=100)
        async with ClientSession(connector=connector) as session:
            await application.bot.set_my_commands(self.commands)

    async def end(self, update: Update, _):
        user = update.effective_user.name
        user_id = update.effective_user.id
        await update.message.reply_text(
            text='Возвращайтесь к нам скорее',
            reply_markup=ReplyKeyboardRemove()
        )
        logger.info("User %s ended chat, id %s", user, user_id)
        return ConversationHandler.END

    async def button(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        try:
            query = update.callback_query
            await query.answer(text=query.data, show_alert=True)
        except Exception as ex:
            logger.error("Exception get callback, %s", ex)

    def run(self):
        application = ApplicationBuilder() \
            .token(self.config['token']) \
            .post_init(self.post_init) \
            .build()

        logger.info("run bot")
        conv_handler = ConversationHandler(
            entry_points=[CommandHandler('start', self.start)],

            states={
                self.CALL_CHANNELS: [
                    MessageHandler(
                        filters.Regex("^(Создать пост)$"), self.channels
                    )
                ],
                self.CALL_CHOISE_CHANNEL: [
                    MessageHandler(
                        filters.Regex("^(Загадки)$"), self.get_post
                    ),
                    MessageHandler(
                        filters.Regex("^(Закончить)$"), self.end
                    )
                ],
                self.CALL_SAVE_POST: [
                    MessageHandler(None, self.save_post)
                ],
                self.CALL_CREATE_POST: [
                    MessageHandler(
                        filters.Regex("^(Добавить кнопку)$"), self.create_post
                    ),
                    MessageHandler(
                        filters.Regex("^(Закончить)$"), self.end
                    )
                ],
                self.CALL_SEND_POST: [
                    MessageHandler(
                        filters.Regex("^(Опубликовать пост)$"), self.send_post
                    ),
                    MessageHandler(
                        filters.Regex("^(Закончить)$"), self.end
                    )
                ],
                self.CALL_NEW_POST: [
                    MessageHandler(
                        filters.Regex("^(Новый пост)$"), self.channels
                    ),
                    MessageHandler(
                        filters.Regex("^(Закончить)$"), self.end
                    )
                ],
                self.CALL_CREATE_POST_: [
                    MessageHandler(
                        filters.Regex("^(Попробовать еще раз)$"), self.channels
                    ),
                    MessageHandler(
                        filters.Regex("^(Закончить)$"), self.end
                    )
                ]
            },
            fallbacks=[CommandHandler('cancel', self.end)],
        )
        application.add_handler(conv_handler)
        application.add_handler(CallbackQueryHandler(self.button))
        application.run_polling()

