import os

from app.consts import Environment

ENVIRONMENT = os.environ.get("ENVIRONMENT")

if ENVIRONMENT == Environment.IFT.value:
    os.environ.setdefault("SET_RIDDLE_CHANNEL_ID", os.environ.get("RIDDLE_ID_IFT"))
elif ENVIRONMENT == Environment.PROD.value:
    os.environ.setdefault("SET_RIDDLE_CHANNEL_ID", os.environ.get("RIDDLE_ID_PROD"))
