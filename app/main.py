import logging
import os

from dotenv import load_dotenv

from app.utils import get_user_id_from_whitelist
from bot import ChatTelegramBot

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)

# set higher logging level for httpx to avoid all GET and POST requests being logged
logging.getLogger("httpx").setLevel(logging.WARNING)
logger = logging.getLogger(__name__)


def create_bot():
    load_dotenv()
    from app import app_config

    telegram_config = {
        'token': os.environ.get('TELEGRAM_BOT_TOKEN'),
        'riddle_chat_id': os.environ.get('SET_RIDDLE_CHANNEL_ID'),
        'whitelist': get_user_id_from_whitelist(os.environ.get('WHITELIST'))
    }

    telegram_bot = ChatTelegramBot(config=telegram_config)
    return telegram_bot


def main():
    bot: ChatTelegramBot = create_bot()
    bot.run()


if __name__ == "__main__":
    main()