import os
import json

def create_riddle_text_post(text: str):
    text = text.replace('.', '\.')
    text = text.replace('-', '\-')
    return """
    *❕ Загадка*\n
""" + text


def split_riddle_answer(text: str) -> list[str]:
    result = text.split("/")
    return result

def get_user_id_from_whitelist(text_list: str):
    user_list = json.loads(text_list)
    return user_list
