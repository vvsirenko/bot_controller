from enum import Enum
from typing import Final


class Environment(Enum):
    PROD: Final[str] = "prod"
    IFT: Final[str] = "ift"